from krita import InfoObject

FILE = '/home/aidin/Documents/krita-bug/02.kra'
SCALE_METHOD = 'Bicubic'
# SCALE_METHOD = 'Kanczos3'

doc = Krita.instance().openDocument(FILE)
doc.setBatchmode(True) # no popups while saving

doc.scaleImage(1512, 2151, 300, 300, SCALE_METHOD)

exportConfig = InfoObject()
exportConfig.setProperty('compression', 9)

exportResult = doc.exportImage(f'{FILE}.png', exportConfig)

if not exportResult:
    print(f'Failed to export [{file_name}]')
else:
    print(f'Exported [{FILE}]')

doc.close()
